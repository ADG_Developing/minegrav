package MineG.engine.sound;

import java.io.File;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class SoundPool {

	public String soundType;
	public String fileType;
	public float soundVolume;
	public boolean isStreamable;
	
	public Map soundPoolEntries = Maps.newHashMap();
	
	public SoundPool(String par1Str, boolean par2){
		this.soundType = par1Str;
		this.isStreamable = par2;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void addSound(String par1Str, String par2Str, float par1) throws MalformedURLException{
		String var2 = par1Str;
		
		this.fileType = "." + par2Str;
		this.soundVolume = par1;
		
        par1Str = par1Str.replace(".", "\\");
        Object var3 = (List)this.soundPoolEntries.get(par1Str);

        if (var3 == null)
        {
            var3 = Lists.newArrayList();
            this.soundPoolEntries.put(par1Str, var3);
        }
        
        ((List) var3).add(new SoundPoolEntry(var2, this.getPath(par1Str), this.getStringPath(par1Str), this.soundVolume));
	}

    private File getPath(String par1Str) throws MalformedURLException
    {
    	String var1 = System.getProperty("user.dir") + "\\assets\\sound\\" + this.soundType + "\\" + par1Str + this.fileType;
        return new File(var1);
    }
    
    private String getStringPath(String par1Str) throws MalformedURLException
    {
    	String var2 = System.getProperty("user.dir") + "\\assets\\sound\\" + this.soundType + "\\" + par1Str + this.fileType;
        return var2;
    }
	
}
