package MineG.engine.sound;

import paulscode.sound.SoundSystem;
import paulscode.sound.SoundSystemConfig;
import paulscode.sound.SoundSystemException;
import paulscode.sound.codecs.CodecJOgg;
import paulscode.sound.codecs.CodecWav;
import paulscode.sound.libraries.LibraryLWJGLOpenAL;

public class SoundManager {

	public static SoundPool soundPoolStreaming = new SoundPool("sound", false);
	public static SoundPool soundPoolMusic = new SoundPool("music", false);
	public static SoundPool soundPoolSound = new SoundPool("streaming", false);
	
	public static SoundSystem soundSystem;
	
	@SuppressWarnings("static-access")
	public void initManager() throws SoundSystemException{
		SoundSystemConfig.addLibrary( LibraryLWJGLOpenAL.class ); 
		SoundSystemConfig.setCodec( "wav", CodecWav.class ); 
		SoundSystemConfig.setCodec( "ogg", CodecJOgg.class ); 
		
		this.soundSystem = new SoundSystem(LibraryLWJGLOpenAL.class);
	}

}
