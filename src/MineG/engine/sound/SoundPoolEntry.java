package MineG.engine.sound;

import java.io.File;
import java.net.MalformedURLException;

import paulscode.sound.SoundSystemConfig;
import MineG.engine.MinyG;

public class SoundPoolEntry{

    private final String soundName;
    private final float soundVolume;
    private final String soundStringUrl;
    private final File soundUrl;

    public SoundPoolEntry(String par1Str, File par2File, String par3StringURL, float par4Str)
    {
        this.soundName = par1Str;
        this.soundUrl = par2File;
        this.soundStringUrl = par3StringURL;
        this.soundVolume = par4Str;
        
        try {
			this.registerEntry();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
    }

    public String getSoundName()
    {
        return this.soundName;
    }
    
    public String getStringURL()
    {
        return this.soundStringUrl;
    }

    public File getSoundUrl()
    {
        return this.soundUrl;
    }
    
    public float getSoundVolume()
    {
        return this.soundVolume;
    }
    
    public void registerEntry() throws MalformedURLException{
    	MinyG.soundManager.soundSystem.newStreamingSource(false, getSoundName(), this.getSoundUrl().toURI().toURL(), this.getStringURL(), false, 0, 0, 0, SoundSystemConfig.ATTENUATION_NONE, 1);
    	MinyG.soundManager.soundSystem.setMasterVolume(this.soundVolume);
    }

}
