/**
 * 
 */
package MineG.engine.scenes;

import java.awt.Graphics;

/**
 * @author ethan
 *
 */
public class Scene {
	
	/** Called when the scene is created.
	 */
	public void start()
	{
		
	}
	/** Called when the scene is switched to.
	 */
	public void switchTo()
	{
		
	}
	/** This method is called every update
	 * @param delta - This is the Delta Time
	 */
	public void update(float delta)
	{
		
	}
	/** This method is called every frame.
	 * You will need to clear OpenGL yourself. If you don't know how , use one of the methods from RenderUtil.
	 * 
	 * @param isRenderThread - This is true if this method is called from the "Render Thread"
	 * @see voxelGame.utils.RenderUtils
	 */
	public void render(boolean isRenderThread)
	{
		
	}
	/**
	 * This will be called when something has requested the scene to change. DO NOT RUN CODE THAT YOU NEED TO DO BEFORE CLOSING YOU SCENE HERE. ONLY CODE TO SEE IF THE SWITCH REQUEST SHOULD BE ALLOWED. All code that must be run before the scene is switch should be in onSwitch().
	 * 
	 * @param stateSaved - True if the instance of this scene will not be deleted
	 * @param requester - The name of the object that is requesting a scene switch
	 * @param code - This is the code that the requester sent with the switch request.
	 * @return Response Code - This is a response code. Different integer mean different things.  </br>
	 * 0 - Allow Switch With Calling onSwitch() </br>
	 * -1 - Do Not Allow Switch </br>
	 * 1 - Allow Switch Without Calling onSwitch() </br>
	 * 2 - Switch when VoxelGame.instance.proceedWithSwitch() is called. (This can be useful if you need to run some code
	 */
	public int switchRequested(boolean stateSaved , String requester , int code)
	{
		
		return 0;
	}
	/** This will be called when the scene is about to switch
	 * 
	 */
	public void onSwitch()
	{
		
	}

}
