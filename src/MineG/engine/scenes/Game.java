package MineG.engine.scenes;

import static org.lwjgl.opengl.GL11.GL_CULL_FACE;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;

import MineG.engine.InitCore;
import MineG.engine.collision.CollideHandler;
import MineG.engine.entity.EntityPlayer;
import MineG.engine.render.Camera;
import MineG.engine.world.World;
import MineG.utils.MathsUtils;
import MineG.utils.RenderUtils;

public class Game extends Scene{
	
	public static Camera playerCam;
	//public static MouseCamera playerMouseCam;
	public static World world;
	public static EntityPlayer player;
	
	public Game()
	{
		//super();
		
		InitCore coreGame = new InitCore();
		coreGame.init();
		
		world = new World();
	}
	
	@Override
	public void switchTo()
	{
		RenderUtils.runOpenGLAction(RenderUtils.GL_LOAD_IDENTITY);
		glEnable(GL_CULL_FACE);
		playerCam = new Camera(70, MathsUtils.getWindowRatiof(), 0.3f, 128);
		player = new EntityPlayer("MrPonyCaptain");
		
		playerCam.setX(0);
		playerCam.setY(-70);
		playerCam.setZ(0);
		playerCam.setRY(135);
	}
	
	@Override
	public void update(float delta)
	{
		//playerMouseCam = new MouseCamera();
		
		CollideHandler aabb = new CollideHandler();
		aabb.checkCollide();
		
		delta = delta / 100000f;
		boolean forward = Keyboard.isKeyDown(Keyboard.KEY_W);
		boolean backward = Keyboard.isKeyDown(Keyboard.KEY_S);
		boolean left = Keyboard.isKeyDown(Keyboard.KEY_D);
		boolean right = Keyboard.isKeyDown(Keyboard.KEY_A);
		
		if(forward)
		{
			playerCam.move(-0.5f * delta , 1);
		}
		if(backward)
		{
			playerCam.move(0.5f  * delta, 1);
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_LEFT))
		{
			playerCam.rotateY(-2f  * delta);
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_RIGHT))
		{
			playerCam.rotateY(2f  * delta);
		}
		
		if(Keyboard.isKeyDown(Keyboard.KEY_UP))
		{
			playerCam.rotateX(-2f  * delta);
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_DOWN))
		{
			playerCam.rotateX(2f  * delta);
		}
		
		if(left)
		{
			playerCam.move(0.5f  * delta, 0);
		}
		if(right)
		{
			playerCam.move(-0.5f  * delta, 0);
		}
		
		if(Keyboard.isKeyDown(Keyboard.KEY_SPACE))
		{
			playerCam.setY(playerCam.getY() - (1 * delta));
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_LSHIFT))
		{
			playerCam.setY(playerCam.getY() + (1 * delta));
		}
		
		if(Keyboard.isKeyDown(Keyboard.KEY_F))
		{
			try {
				Display.setFullscreen(true);
				Display.update();
			} catch (LWJGLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	@Override
	public void render(boolean isRenderThread)
	{
		RenderUtils.runOpenGLAction(RenderUtils.CLEAR_ALL);
		
		glPushMatrix();
		{	
			playerCam.useView();
			world.render();
		}
		glPopMatrix();

	}

}
