package MineG.engine.collision;

public interface AABBObject
{
    public AABB getAABB();
}
