package MineG.engine;

import MineG.engine.blocks.Block;
import MineG.engine.items.Item;

public class InitCore {

	public static int stackSize = 128;
	public static float gravity = 1F;
	public static float weightMultiplier = 1.2F;
	
	public void init(){
		Block block = new Block();
		
		Item item = new Item();
		item.addItems();
	}
	
}
