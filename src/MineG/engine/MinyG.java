package MineG.engine;

import java.net.MalformedURLException;

import org.lwjgl.opengl.Display;

import paulscode.sound.SoundSystemException;
import MineG.engine.render.textures.EasyTexture;
import MineG.engine.render.textures.TextureEngine;
import MineG.engine.scenes.Game;
import MineG.engine.scenes.Scene;
import MineG.engine.sound.SoundManager;

public class MinyG {
	//The instance of the game
	public static MinyG instance;
	//The instance of the logger
	public static LogAgent MinyLogger;

	//The instance of the logger
	public static SoundManager soundManager = new SoundManager();
	
	public Thread renderThreadThread;	
	public Scene game ;
	
	@SuppressWarnings("static-access")
	public MinyG(String[] args)  {
		MinyLogger = new LogAgent("[MinyP]", true, 8);
		MinyLogger.logInfo("MinyP starting up!");
		MinyLogger.logInfo("Running init for LWJGL");
		
		initLWJGL();
		
		MinyLogger.logInfo("Creating Window");
		
		Window.createWindow(800, 600, "MinyP");
		TextureEngine.instance = new TextureEngine();
		EasyTexture.instance = new EasyTexture();
		
		try {
			soundManager.initManager();
			
			soundManager.soundPoolMusic.addSound("core.TFGrw - Forge Of LegendsFULL", "wav", 4F);
			soundManager.soundSystem.play("core.TFGrw - Forge Of LegendsFULL");
		} catch (SoundSystemException | MalformedURLException e) {
			e.printStackTrace();
		}
	}
	
	private boolean exitRequest = false;
	private int fps;
	private int lastFPS;
	
	private void gameLoop() {
		game  = new Game();
		Scene current = game;
		
		current.switchTo();
		lastFPS = (int) getTime();
		float oldTimeSinceStart = System.nanoTime() / 1000;
		while(!exitRequest)
		{
		    float timeSinceStart = System.nanoTime() / 1000;
		    float deltaTime = timeSinceStart - oldTimeSinceStart;
		    oldTimeSinceStart = timeSinceStart;
		     
			current.update(deltaTime);
			current.render(false);
			
			if(Window.isCloseRequested()){
				exitRequest = true;
			}
			
			Window.render();
			updateFPS();
			
			if(exitRequest){
				instance.soundManager.soundSystem.stop("core.TFGrw - Forge Of LegendsFULL");
				Window.dispose();
				System.exit(0);
			}
		}
		
	}

	private void initLWJGL() {
        String OS = System.getProperty("os.name");
        if (OS.toLowerCase().contains("windows")) 
        	OS = "windows";
        else if (OS.toLowerCase().contains("mac")) 
        	OS = "macosx";
        else if (OS.toLowerCase().contains("linux")) 
        	OS = "linux";
        else 
        	OS = "Error";
        
        if (OS == "Error")
        {
        	MinyLogger.logInfo("Sorry. It apears that you are running an Unsuported operating system");
        	System.exit(1);
        }
        
        MinyLogger.logInfo("Setting property \"org.lwjgl.librarypath\" to \""+System.getProperty("user.dir") + "\\LuxEmita_lib\\native\\" + OS);
        System.setProperty("org.lwjgl.librarypath",System.getProperty("user.dir") + "\\VoxelGame_lib\\native\\" + OS);    
		
	}

	public static void main(String[] args) {		
		instance = new MinyG(args);
		instance.gameLoop();
	}

	public void requestExit() {
		this.exitRequest = true;
	}
	
	public void updateFPS() {
	    
		if (getTime() - lastFPS > 1000) {
	        Display.setTitle("FPS: " + fps); 
	        fps = 0; //reset the FPS counter
	        lastFPS += 1000; //add one second
	    }
	    fps++;
	}
	public long getTime() {
	    return System.nanoTime()/1000000;
	}
	
	public LogAgent getMinyLogger()
	{
		return MinyLogger;
	}

}
