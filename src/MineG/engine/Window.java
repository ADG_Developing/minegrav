package MineG.engine;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

public class Window 
{
	/**creates a window*/
	public static void createWindow(int width, int height, String title)
	{
		Display.setTitle(title);
		
		try 
		{
			//Display.setDisplayMode(new DisplayMode(width, height));
			Display.setResizable(true);
			
			Display.create();
			Keyboard.create();
			Mouse.create();
		} 
		catch (LWJGLException e) 
		{
			e.printStackTrace();
		}
	}
	/**updates the window*/
	public static void render()
	{	
		Display.update();
	}
	/** Destroys the window*/
	public static void dispose()
	{
		Display.destroy();
		Keyboard.destroy();
		Mouse.destroy();
	}
	/**If the close button is pressed*/
	public static boolean isCloseRequested()
	{
		return Display.isCloseRequested();
	}
	/**Width of Window*/
	public static int getWidth()
	{
		return Display.getDisplayMode().getWidth();
	}
	/**Height of Window*/
	public static int getHeight()
	{
		return Display.getDisplayMode().getHeight();
	}
	
	public static String getTitle()
	{
		return Display.getTitle();
	}
}
