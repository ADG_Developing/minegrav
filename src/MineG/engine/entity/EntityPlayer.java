package MineG.engine.entity;

import MineG.engine.entity.model.ModelPlayer;
import MineG.engine.scenes.Game;

public class EntityPlayer {

	public float xPos;
	public float yPos;
	public float zPos;
	
	public float gravity;
	
	public String playerName;
	
	public EntityPlayer(String par1Str){
		this.gravity = 1F;

		this.playerName = par1Str;
		
		this.xPos = Game.playerCam.getX();
		this.yPos = Game.playerCam.getY();
		this.zPos = Game.playerCam.getZ();
		
		ModelPlayer.renderModel((int)xPos, (int)yPos, (int)zPos);
	}
	
	public void updateYPos(){
		//Game.playerCam.setY(yPos += gravity);
	}
	
}
