package MineG.engine.entity.model;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glColor3f;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glVertex3f;

import org.lwjgl.opengl.GL11;

import MineG.engine.scenes.Game;

public class ModelPlayer {

	public static void renderModel(int x, int y, int z){
		glPushMatrix();
		{
			GL11.glTranslatef(x,y,z);
			
			glColor3f(1.0f,1.0f,1.0f);
			//glBindTexture(GL_TEXTURE_2D, setBlockTexture(textureName));
			glBegin(GL_QUADS);
			{
				glTexCoord2f(1, 1);
				glVertex3f(1,0,1);
				
				glTexCoord2f(1, 0);
				glVertex3f(1,1,1);
				
				glTexCoord2f(0, 0);
				glVertex3f(0,1,1);
				
				glTexCoord2f(0, 1);
				glVertex3f(0,0,1);

				glTexCoord2f(1, 1);
				glVertex3f(0,0,0);
				
				glTexCoord2f(1, 0);
				glVertex3f(0,1,0);
				
				glTexCoord2f(0, 0);
				glVertex3f(1,1,0);
				
				glTexCoord2f(0, 1);
				glVertex3f(1,0,0);

				glTexCoord2f(1, 1);
				glVertex3f(0,0,1);
				
				glTexCoord2f(1, 0);
				glVertex3f(0,1,1);
				
				glTexCoord2f(0, 0);
				glVertex3f(0,1,0);
				
				glTexCoord2f(0, 1);
				glVertex3f(0,0,0);
				
				glTexCoord2f(0, 0);
				glVertex3f(1,1,1);
				
				glTexCoord2f(0, 1);
				glVertex3f(1,0,1);
				
				glTexCoord2f(1, 1);
				glVertex3f(1,0,0);
				
				glTexCoord2f(1, 0);
				glVertex3f(1,1,0);

				glTexCoord2f(1, 1);
				glVertex3f(0,0,0);
				
				glTexCoord2f(1, 0);
				glVertex3f(1,0,0);
				
				glTexCoord2f(0, 0);
				glVertex3f(1,0,1);
				
				glTexCoord2f(0, 1);
				glVertex3f(0,0,1);

				glTexCoord2f(1, 1);
				glVertex3f(1,1,0);
				
				glTexCoord2f(1, 0);
				glVertex3f(0,1,0);
				
				glTexCoord2f(0, 0);
				glVertex3f(0,1,1);
				
				glTexCoord2f(0, 1);
				glVertex3f(1,1,1);			
			}
			glEnd();
		}
		glPopMatrix();
	}
	
}
