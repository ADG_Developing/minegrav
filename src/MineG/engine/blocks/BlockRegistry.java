package MineG.engine.blocks;

import MineG.engine.Material;

public class BlockRegistry {

	public static void registerBlock(int blockID, Block block, Material material){
		block.blockID = blockID;
		block.material = material;
	}
	
}
