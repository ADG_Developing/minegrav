package MineG.engine.blocks;

import MineG.engine.Material;
import MineG.engine.collision.AABB;
import MineG.engine.collision.AABBObject;
import MineG.engine.render.RenderBlock;

public class Block implements AABBObject {

	public int blockID;
	public int[][][][] lightValue;
	
	public String textureName;
	public String unlocalizedName;
	public String blockName;
	
	public Material material;
	
	public AABB aabb;
	
	public static final Block[] blockList = new Block[409600];

    public static final Block dirt = (new BlockDirt(1, Material.ground)).setUnlocalizedName("testdirt").setTextureName("testdirt").renderBlock(true);
    public static final Block pluto = (new Block(2, Material.ground)).setUnlocalizedName("testplutoniumore").setTextureName("testplutoniumore").renderBlock(true);
    public static final Block stone = (new BlockStone(3, Material.ground)).setUnlocalizedName("teststone").setTextureName("teststone").renderBlock(true);
	
	public Block(int par1, Material material){
        if (blockList[(par1 + 40960)] != null)
        {
            throw new IllegalArgumentException("Slot " + (par1 + 40960) + " is already occupied by " + blockList[par1 + 40960] + " when adding " + this);
        }
        else
        {
    		this.blockID = par1 + 40960;
    		blockList[par1 + 40960] = this;
    		
            if(material != null){
            	this.material = material;
            }
        }
	}
	
	public Block(){
	
	}
	
	protected Block setTextureName(String par1){
		RenderBlock.setBlockTexture(par1);
		this.textureName = par1;	
		return this;
	}
	
	protected Block setUnlocalizedName(String par1){
		this.unlocalizedName = par1;
		return this;
	}
	
	protected Block setName(String par1){
		this.blockName = par1;
		return this;
	}
	
	protected Block renderBlock(boolean par1){
		RenderBlock.setDoRender(par1);
		return this;
	}
	
	protected int lightValue(int par1, int par2, int par3, int par4){
		return this.lightValue[par1][par2][par3][par4];
	}
	
	public Block getData(){
	    return null;
	}
	
}