package MineG.engine;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class LogAgent extends Logger {
	
	public String output;
	public boolean isLogger;
	public int bitRate;
	
	public LogAgent(String output, boolean isLogger, int bitRate){
		setOutput(output);
		
		this.isLogger = isLogger;
		this.bitRate = bitRate;
	}
	
	private void setOutput(String output){		
		this.output = output;
	}
	
	public void logInfo(String msg){		
		if(this.isLogger && this.bitRate >= 8 && this.bitRate <= 256){		
			
	    	Calendar cal = Calendar.getInstance();
	    	cal.getTime();
	    	SimpleDateFormat sdf = new SimpleDateFormat("dd-mm-yyyy - HH:mm:ss");			
	    	
			System.out.println("[" + sdf.format(cal.getTime()) + "]" + this.output + " " + msg);
		}
	}
	
}
