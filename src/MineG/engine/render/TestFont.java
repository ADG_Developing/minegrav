package MineG.engine.render;

import java.awt.Font;

public class TestFont {

	public TextRenderer textRenderer;
	
	public void addText(String msg){
		Font font = new Font("Serif", Font.BOLD, 32);
		textRenderer = new TextRenderer(font, true);
		textRenderer.drawString(100, 100, msg, 10, 10);
		textRenderer.destroy();
	}
	
}
