package MineG.engine.render;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glColor3f;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glTranslatef;
import static org.lwjgl.opengl.GL11.glVertex3f;
import MineG.engine.blocks.Block;
import MineG.engine.render.textures.EasyTexture;
import MineG.engine.scenes.Game;
import MineG.engine.world.World;

public class RenderBlock {

	public static boolean doRender = false;
	
	public static void setDoRender(boolean getDoRender){
		if(getDoRender){
			doRender = true;
		}else{
			doRender = false;
		}
	}
	
	public static void renderBlock(int x, int y, int z, Block block){
		String textureName = block.textureName;
		//System.out.println(block.textureName + ", " + block.unlocalizedName);
		glPushMatrix();
		{	
			glTranslatef(x,y,z);
			
			glColor3f(1.0f,1.0f,1.0f);
			glBindTexture(GL_TEXTURE_2D, setBlockTexture(textureName));
			glBegin(GL_QUADS);
			{
				//Z Plane +  = Yellow
				if(World.getBlock(x,y,z + 1) == 0)
				{
				
				//glBegin(GL_QUADS);
				glTexCoord2f(1, 1);
				glVertex3f(1,0,1);
				
				glTexCoord2f(1, 0);
				glVertex3f(1,1,1);
				
				glTexCoord2f(0, 0);
				glVertex3f(0,1,1);
				
				glTexCoord2f(0, 1);
				glVertex3f(0,0,1);
				
				//glEnd();
				
				}
				//Z Plane -  = Blue
				if(Game.world.getBlock(x,y,z - 1) == 0)
				{
				
				//glBegin(GL_QUADS);
				glTexCoord2f(1, 1);
				glVertex3f(0,0,0);
				
				glTexCoord2f(1, 0);
				glVertex3f(0,1,0);
				
				glTexCoord2f(0, 0);
				glVertex3f(1,1,0);
				
				glTexCoord2f(0, 1);
				glVertex3f(1,0,0);
				
				//glEnd();
				
				}
				
				if(Game.world.getBlock(x - 1,y,z) == 0)
				{
				
				//glBegin(GL_QUADS);
				glTexCoord2f(1, 1);
				glVertex3f(0,0,1);
				
				glTexCoord2f(1, 0);
				glVertex3f(0,1,1);
				
				glTexCoord2f(0, 0);
				glVertex3f(0,1,0);
				
				glTexCoord2f(0, 1);
				glVertex3f(0,0,0);
				
				//glEnd();
				
				}
				
				if(Game.world.getBlock(x + 1,y,z) == 0)
				{
				
				//glBegin(GL_QUADS);
				glTexCoord2f(0, 0);
				glVertex3f(1,1,1);
				
				glTexCoord2f(0, 1);
				glVertex3f(1,0,1);
				
				glTexCoord2f(1, 1);
				glVertex3f(1,0,0);
				
				glTexCoord2f(1, 0);
				glVertex3f(1,1,0);
				
				//glEnd();
				}
				
				if(Game.world.getBlock(x,y - 1,z) == 0)
				{
				
				//glBegin(GL_QUADS);
				glTexCoord2f(1, 1);
				glVertex3f(0,0,0);
				
				glTexCoord2f(1, 0);
				glVertex3f(1,0,0);
				
				glTexCoord2f(0, 0);
				glVertex3f(1,0,1);
				
				glTexCoord2f(0, 1);
				glVertex3f(0,0,1);
				
				//glEnd();
				
				}								
				
				if(Game.world.getBlock(x,y + 1,z) == 0)
				{
				
				//glBegin(GL_QUADS);
				glTexCoord2f(1, 1);
				glVertex3f(1,1,0);
				
				glTexCoord2f(1, 0);
				glVertex3f(0,1,0);
				
				glTexCoord2f(0, 0);
				glVertex3f(0,1,1);
				
				glTexCoord2f(0, 1);
				glVertex3f(1,1,1);
				
				//glEnd();
				
				}
			}
			glEnd();
		}
		glPopMatrix();
	}
	
	public static int setBlockTexture(String path){
		int StonePointer = EasyTexture.instance.getBindingIntForURL(System.getProperty("user.dir") + "\\res\\textures\\" + path + ".png" , false);
	    return StonePointer;
	}
	
}
