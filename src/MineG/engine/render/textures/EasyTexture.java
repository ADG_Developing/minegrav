package MineG.engine.render.textures;

import java.util.HashMap;
import java.util.Map;

import org.lwjgl.opengl.GL11;

/** This is a some what simpler texture engine. Use this for basic texture drawing and loading.**/
public class EasyTexture {
	/** A map that is for URL to bounding texture conversion**/
	Map<String , Integer> textures;
	public static EasyTexture instance;
	public EasyTexture()
	{
		textures = new HashMap<String , Integer>();
		
		//VoxelGame.instance.VoxelLogger.logInfo("Easy Texture Engine Instance Made.");
				
	}
	
	@SuppressWarnings("deprecation")
	public int getBindingIntForURL(String URL)
	{
		if (textures.containsKey(URL))
		{
			return textures.get(URL);
		}
		else
		{
				textures.put(URL, TextureEngine.instance.loadTexture(TextureEngine.instance.loadImage(URL), GL11.GL_LINEAR));
			return textures.get(URL);
		}
	}
	
	@SuppressWarnings("deprecation")
	public int getBindingIntForURL(String URL , boolean useGLLinear)
	{
		if (textures.containsKey(URL))
		{
			return textures.get(URL);
		}
		else
		{
			int Type = GL11.GL_NEAREST;
			if (useGLLinear)
				Type = GL11.GL_LINEAR;
				textures.put(URL, TextureEngine.instance.loadTexture(TextureEngine.instance.loadImage(URL), Type));
			return textures.get(URL);
		}
	}
}
