package MineG.engine.render;

import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_MODELVIEW;
import static org.lwjgl.opengl.GL11.GL_PROJECTION;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glMatrixMode;
import static org.lwjgl.opengl.GL11.glRotatef;
import static org.lwjgl.opengl.GL11.glTranslatef;
import static org.lwjgl.util.glu.GLU.gluPerspective;
import MineG.libs.maths.Vector3f;

public class Camera {
	
	private Vector3f rotation;
	private Vector3f position;
	
	float fov , aspect , near , far ;
	
	public boolean lockMoveX;
	public boolean lockMoveY;
	public boolean lockMoveZ;
	
	public Camera(float fov , float aspect , float near , float far)
	{
		position = new Vector3f(0, 0, 0);
		rotation = new Vector3f(0, 0, 0);
		
		this.fov = fov;
		this.aspect = aspect;
		this.near = near;
		this.far = far;
		
		initProjection();
	}
	
	public float getX() {
		return position.getX();
	}

	public void setX(float x) {
		if(this.lockMoveX){
			
		}else{
			this.position.setX(x);
		}
	}
	
	public float getY() {
		return position.getY();
	}

	public void setY(float y) {
		if(this.lockMoveY){
			
		}else{
			this.position.setY(y);
		}
	}
	
	public float getZ() {
		return position.getZ();
	}

	public void setZ(float z) {
		if(this.lockMoveZ){
			
		}else{
			this.position.setZ(z);
		}
	}

	public float getRX() {
		return rotation.getX();
	}

	public void setRX(float rx) {
		this.rotation.setX(rx);
	}

	public float getRY() {
		return rotation.getY();
	}

	public void setRY(float ry) {
		this.rotation.setY(ry);
	}
	
	public float getRZ() {
		return rotation.getZ();
	}

	public void setRZ(float rz) {
		this.rotation.setZ(rz);
	}
	
	public Vector3f getRotation() {
		return rotation;
	}

	public void setRotation(Vector3f rotation) {
		this.rotation = rotation;
	}

	public Vector3f getPosition() {
		return position;
	}

	public void setPosition(Vector3f position) {
		this.position = position;
	}

	public float getFov() {
		return fov;
	}

	public void setFov(float fov) {
		this.fov = fov;
	}

	public float getAspect() {
		return aspect;
	}

	public void setAspect(float aspect) {
		this.aspect = aspect;
	}

	public float getNear() {
		return near;
	}

	public void setNear(float near) {
		this.near = near;
	}

	public float getFar() {
		return far;
	}

	public void setFar(float far) {
		this.far = far;
	}
	
	public void useView()
	{
		glRotatef(rotation.getX(),1,0,0);
		glRotatef(rotation.getY(),0,1,0);
		glRotatef(rotation.getZ(),0,0,1);
		glTranslatef(position.getX(),position.getY(),position.getZ());
	}
	
	public void move(float amt , float dir) 
	{	
		position.setZ((float) (position.getZ() - (amt * Math.sin(Math.toRadians(rotation.getY() + 90 * dir)))));
		position.setX((float) (position.getX() - (amt * Math.cos(Math.toRadians(rotation.getY() + 90 * dir)))));
	}
	
	public void rotateY(float amt)
	{
		rotation.setY((float) (rotation.getY() + amt));
	}
	public void rotateX(float amt)
	{
		rotation.setX((float) (rotation.getX() + amt));
	}
	
	public void rotateZ(float amt)
	{
		rotation.setZ((float) (rotation.getZ() + amt));
	}
	
	private void initProjection()
	{
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(fov,aspect,near,far);
		glMatrixMode(GL_MODELVIEW);
		
		glEnable(GL_DEPTH_TEST);
	}

}
