package MineG.engine.world.biome;

import java.util.Map;

import MineG.engine.blocks.Block;
import MineG.engine.world.dimension.Dimension;

public class Biome {
	
	public static int biomeID;
	public static String biomeName;
	public static BiomeGenBase genFile;
	
	public static Block topBlock;
	public static Block fillerBlock;
	public static Dimension dimension;
	
	public static Map<Block, String> liquids;
	
	@SuppressWarnings("static-access")
	public Biome(int biomeID, String biomeName, BiomeGenBase genFile){
		this.biomeID = biomeID;
		this.biomeName = biomeName;
		this.genFile = genFile;
	}
	
}