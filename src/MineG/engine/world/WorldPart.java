package MineG.engine.world;

import static org.lwjgl.opengl.GL11.GL_COMPILE;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glCallList;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glEndList;
import static org.lwjgl.opengl.GL11.glGenLists;
import static org.lwjgl.opengl.GL11.glNewList;
import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;
import MineG.engine.blocks.Block;
import MineG.engine.render.RenderBlock;
import MineG.engine.render.textures.EasyTexture;

public class WorldPart {
	
	private byte[][][] worldByte;
	
	static final int partSizeX = 64;
	static final int partSizeY = 128;
	static final int partSizeZ = 64;
	
	private int xPart;
	private int yPart;
	private int zPart;
	
	public WorldPart(int x , int y, int z, World world){
		xPart = x;
		yPart = y;
		zPart = z;
		
		worldByte = new byte[partSizeX][partSizeY][partSizeZ];
	}
	
	public void generate(long seed, World world){	
		String worldString = "";
		SimplexNoise.genGrad(seed);
		for(int z = 0; z < partSizeZ ; z ++)
		{	
			for(int x = 0; x < partSizeX ; x ++)
			{
				int height = (int) (SimplexNoise.noise(((double)x/(double)32) + ((double)xPart*(double)partSizeX), (double)z/(double)32)* 16) + 50;
				worldString = worldString + height + " , ";
				for(int y = 0; y < partSizeY ; y ++)
				{
					if(y < height)
						worldByte[x][y][z] = 1;
					else
						worldByte[x][y][z] = 0;
					
				}
			}
			
			worldString = worldString + "\n";
		}
		
		this.makeRenderList(world);
	}
	
	public long getX() {
		return xPart;
	}
	
	public long getY() {
		return yPart;
	}
	
	public long getZ() {
		return zPart;
	}
	
	public void render()
	{		
		glCallList(worldPointer);
	}
	
	private int worldPointer;
	
	public void makeRenderList(World world)
	{
		worldPointer = glGenLists (1);
		glNewList(worldPointer , GL_COMPILE);
		
		//int StonePointer = EasyTexture.instance.getBindingIntForURL(System.getProperty("user.dir") + "\\res\\textures\\testdirt.png" , false);
			
		glPushMatrix();
		{	
			glEnable(GL_TEXTURE_2D);
			for(int y = 0; y < partSizeY ; y ++)
			{	
				for(int x = 0; x < partSizeX ; x ++)
				{
					for(int z = 0; z < partSizeZ ; z ++)
					{
						if (world.getBlock(x,y,z) == 1 &&(world.getBlock(x+1,y,z) == 0||world.getBlock(x,y+1,z) == 0||world.getBlock(x,y,z+1) == 0||world.getBlock(x-1,y,z) == 0||world.getBlock(x,y-1,z) == 0||world.getBlock(x,y,z-1) == 0 && RenderBlock.doRender == true))
						{
							RenderBlock.renderBlock(x, y, z, Block.stone);
						}
					}
				}
			}
			
			for(int y = 0; y < partSizeY ; y ++)
			{	
				for(int x = 0; x < partSizeX ; x ++)
				{
					for(int z = 0; z < partSizeZ ; z ++)
					{
						if (world.getBlock(x,y,z) == 1 &&(world.getBlock(x+1,y,z) == 0||world.getBlock(x,y+1,z) == 0||world.getBlock(x,y,z+1) == 0||world.getBlock(x-1,y,z) == 0||world.getBlock(x,y-1,z) == 0||world.getBlock(x,y,z-1) == 0 && RenderBlock.doRender == true))
						{
							RenderBlock.renderBlock(x, y + 2, z, Block.dirt);
						}
					}
				}
			}
		}
		
		glPopMatrix();
		glEndList();
	}
	
}
