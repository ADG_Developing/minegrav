package MineG.engine.world;

import static org.lwjgl.opengl.GL11.GL_COMPILE;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glCallList;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glEndList;
import static org.lwjgl.opengl.GL11.glGenLists;
import static org.lwjgl.opengl.GL11.glNewList;
import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;

import java.util.Random;

import MineG.engine.blocks.Block;
import MineG.engine.render.RenderBlock;
import MineG.engine.render.textures.EasyTexture;

public class World {
	
	private static byte[][][] worldByte;
	private Random rand;
	private static final int worldSizeX = 16 , worldSizeY = 256 , worldSizeZ = 16;
	
	//public WorldPartContainer parts;
	
	public WorldPart wPart;
	
	public World()
	{
		worldByte = new byte[worldSizeX][worldSizeY][worldSizeZ];
		rand = new Random();
		String worldString = "";
		
		//this.parts = new WorldPartContainer();
		
		SimplexNoise.genGrad(rand.nextInt());
		for(int z = 0; z < worldSizeZ ; z ++)
		{	
			for(int x = 0; x < worldSizeX ; x ++)
			{
				int height = (int) (SimplexNoise.noise((double)x/(double)32, (double)z/(double)32)* 16) + 50;
				worldString = worldString + height + " , ";
				for(int y = 0; y < worldSizeY ; y ++)
				{
					if(y < height)
						worldByte[x][y][z] = 1;
					else
						worldByte[x][y][z] = 0;
					
				}
			}
			
			worldString = worldString + "\n";
		}
		
		//MinyP.instance.getMinyLogger().logInfo(worldString);
		updateRenderList();
	}
	
	public static int getBlock(int x , int y , int z)
	{
		if(x >= 0 && x < worldSizeX && y >= 0 && y < worldSizeY && z >= 0 && z < worldSizeZ)
		{
			return worldByte[x][y][z];
		}
		else
		{
			
			return 0;
		}
	}
	
	public void render()
	{		
		glCallList(worldPointer);
	}
	
	private int worldPointer;
	
	private void updateRenderList()
	{
		worldPointer = glGenLists (1);
		glNewList(worldPointer , GL_COMPILE);
		
		int StonePointer = EasyTexture.instance.getBindingIntForURL(System.getProperty("user.dir") + "\\res\\textures\\testdirt.png" , false);
			
		glPushMatrix();
		{	
			glEnable(GL_TEXTURE_2D);
			for(int y = 0; y < worldSizeY ; y ++)
			{	
				for(int x = 0; x < worldSizeX ; x ++)
				{
					for(int z = 0; z < worldSizeZ ; z ++)
					{
						if (getBlock(x,y,z) == 1 &&(getBlock(x+1,y,z) == 0||getBlock(x,y+1,z) == 0||getBlock(x,y,z+1) == 0||getBlock(x-1,y,z) == 0||getBlock(x,y-1,z) == 0||getBlock(x,y,z-1) == 0 && RenderBlock.doRender == true))
						{
							RenderBlock.renderBlock(x, y, z, Block.stone);
						}
					}
				}
			}
			
			for(int y = 0; y < worldSizeY ; y ++)
			{	
				for(int x = 0; x < worldSizeX ; x ++)
				{
					for(int z = 0; z < worldSizeZ ; z ++)
					{
						if (getBlock(x,y,z) == 1 &&(getBlock(x+1,y,z) == 0||getBlock(x,y+1,z) == 0||getBlock(x,y,z+1) == 0||getBlock(x-1,y,z) == 0||getBlock(x,y-1,z) == 0||getBlock(x,y,z-1) == 0 && RenderBlock.doRender == true))
						{
							RenderBlock.renderBlock(x, y + 2, z, Block.dirt);
						}
					}
				}
			}
		}
		
		glPopMatrix();
		glEndList();
		
		/*for(int x = 0; x < wPart.partSizeX / 4 ; x ++){
			for(int z = 0; z < wPart.partSizeZ / 4 ; z ++){
				this.wPart = new WorldPart(x, 0, z, this);
				this.wPart.generate(1, this);
			}
		}
		
		this.wPart.generate(1, this);*/
	}

}