package MineG.engine;

public class Material {

	public static Material stone;
	public static Material ground;
	public static Material glass;
	public static Material air;
	public static Material grass;
	public static Material wood;
	public static Material leaf;
	public static Material ore;
	public static Material oreBlock;
	
	public Material(){
		stone();
	}
	
	public Material(String par1str, float par2, float par3, float par4){
		float hardness = par2;
		float collision = par3;
		float opacity = par4;
	}
	
	public static Material stone(){
		stone = new Material("Stone", 1f, 1f, 1f);
		return stone;
	}
	
	public static Material ground(){
		stone = new Material("Ground", 1f, 1f, 1f);
		return stone;
	}
	
}
