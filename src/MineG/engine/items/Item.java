package MineG.engine.items;

import MineG.engine.Material;

public class Item {

	public static int itemID;	
	public static int[][][][] lightValue;
	
	public static String textureName;
	public static String unlocalizedName;
	public static String blockName;
	
	public static Item itemBlock;
	public static Item pickaxe;
	public static final Item[] itemList = new Item[409600];
	
	public static Material material;
	
	public void addItems(){
		itemBlock = new ItemBlock(1);
		pickaxe = new Item(2);
	}
	
	public Item(int par1){
        if (itemList[par1] != null)
        {
            throw new IllegalArgumentException("Slot " + par1 + " is already occupied by " + itemList[par1] + " when adding " + this);
        }
        else
        {
            itemList[par1] = this;
            this.itemID = par1;
        }
	}
	
	public Item(){
		
	}
	
	protected Item setTextureName(String par1){
		this.textureName = par1;
		return this;
	}
	
	protected Item setUnlocalizedName(String par1){
		this.unlocalizedName = par1;
		return this;
	}
	
	protected Item setName(String par1){
		this.blockName = par1;
		return this;
	}
	
	protected int lightValue(int par1, int par2, int par3, int par4){
		return this.lightValue[par1][par2][par3][par4];
	}
	
}
