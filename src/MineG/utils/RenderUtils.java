/**
 * 
 */
package MineG.utils;

import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_MODELVIEW;
import static org.lwjgl.opengl.GL11.GL_PROJECTION;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glMatrixMode;
import static org.lwjgl.opengl.GL11.glOrtho;
import MineG.engine.Window;
/**
 * 
 * These are useful methods to help with rendering.
 * @author ethan
 *
 */
public class RenderUtils {
	
	public static final int CLEAR_ALL = 0;
	public static final int INIT_OPENGL_FOR_2D_ONLY = 1;
	/**This basically clears the OpenGL mode*/
	public static final int GL_LOAD_IDENTITY = 2;
	
	public static void runOpenGLAction(int action)
	{
		
		switch(action)
		{
		case CLEAR_ALL:
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			
		break;
		case INIT_OPENGL_FOR_2D_ONLY:
			glMatrixMode(GL_PROJECTION);
    		glLoadIdentity();
    		glOrtho(0,Window.getWidth(),Window.getHeight(),0,1,-1);
    		glMatrixMode(GL_MODELVIEW);
    	break;
		case GL_LOAD_IDENTITY:
			glMatrixMode(GL_PROJECTION);
	    	glLoadIdentity();
	    
	    	glMatrixMode(GL_MODELVIEW);
	    break;
			
			
		}
	}

}
