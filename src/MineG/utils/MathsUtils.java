/**
 * 
 */
package MineG.utils;

import MineG.engine.Window;

/**
 * @author ethan
 *
 */
public class MathsUtils {
	
	/** This finds out the ratio of the window
	 *  @return The ratio of width against height of the window as a float*/
	public static float getWindowRatiof()
	{
		return (float)Window.getWidth()/(float)Window.getHeight();
	}
	/** This finds out the ratio of the window
	 * @return The ratio of width against height of the window as a double*/
	public static double getWindowRatiod()
	{
		return (double)Window.getWidth()/(double)Window.getHeight();
	}
	

	public static boolean arrayContains(float[] array, float cont) {
		boolean doesContain = false;
		for(int n = 0 ; n < array.length ; n++ )
		{
			if(array[n] == cont)
				doesContain = true;
		}
		return doesContain;
		
	}
	
	public static int tuntr(int input){
	    int output;
		
		output = Maths.cos(Maths.sin(input));
		
		for(int o = 0; n <input.length; n++){
		    output += Maths.tan(n);
		}
		
		return output;
	}

}
