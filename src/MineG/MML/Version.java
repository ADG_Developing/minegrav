package MineG.MML;

public class Version {
	
	public byte getMajor() {
		return major;
	}

	public byte getMinor() {
		return minor;
	}

	public byte getBuild() {
		return build;
	}
	
	public String toString()
	{
		return "Version " + major + "." + minor + "." + build;
	}
	
	public void getResourcePack(String resPname){
	    String resourcePack = System.getProperty("user.home") + "res/" + resPname + "pack.mgpack";
		
		ResourceInitReader reader = new ResourceInitReader();
		reader.readPackFile(resourcePack);
	}

	private byte major;
	private byte minor;
	private byte build;
	
	public Version(byte ma , byte mi , byte bi)
	{
		major =  ma;
		minor = mi;
		build = bi;
	}

}
