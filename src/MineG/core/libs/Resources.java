/**
 * 
 */
package MineG.core.libs;

import MineG.MML.Version;

/**
 * @author ethan
 *
 */
public class Resources {

	public static final String CORE_NAME = "MinyP";
	public static final Version CURRENT_VERSION = new Version((byte)0,(byte)0,(byte)2);
}
